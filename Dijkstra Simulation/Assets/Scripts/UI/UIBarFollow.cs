﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIBarFollow : MonoBehaviour {
    void LateUpdate() {
        transform.position = new Vector3(transform.parent.position.x, transform.position.y, transform.parent.position.z);
        transform.rotation = Quaternion.identity;
    }
}
