﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour {
    private Player player;
    [SerializeField]
    private Image healthBar;
    [SerializeField]
    private Image ammoBar;

    void Awake() {
        player = transform.parent.parent.GetComponent<Player>();
    }

    void Update() {
        healthBar.fillAmount = player.GetHealthPercentage();
        ammoBar.fillAmount = player.GetAmmoPercentage();
    }
}
