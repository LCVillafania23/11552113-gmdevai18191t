﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyUI : MonoBehaviour {
    private Enemy enemy;
    private Image healthBar;

    void Awake()
    {
        enemy = GetComponentInParent<Enemy>();
        healthBar = GetComponent<Image>();
    }

    void Update() {
        healthBar.fillAmount = enemy.GetHealthPercentage();
    }
}
