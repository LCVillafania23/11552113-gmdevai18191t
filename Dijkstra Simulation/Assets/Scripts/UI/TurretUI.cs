﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurretUI : MonoBehaviour {
    private Turret turret;
    private Image ammoBar;

    void Awake() {
        turret = GetComponentInParent<Turret>();
        ammoBar = GetComponent<Image>();
    }

    void Update() {
        ammoBar.fillAmount = turret.GetAmmoPercentage();
    }
}
