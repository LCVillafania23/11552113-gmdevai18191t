﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretSpawner : MonoBehaviour {
    public List<Turret> TurretTypes = new List<Turret>();
    public List<Turret> PooledTurrets = new List<Turret>();

    [SerializeField]
    protected List<GameObject> terrainPositions = new List<GameObject>();
    [SerializeField]
    protected List<GameObject> aerialPositions = new List<GameObject>();
    [SerializeField]
    protected Vector3 terrainOffset;
    [SerializeField]
    protected Vector3 aerialOffset;

    private List<Vector3> TerrainSpawnPoints = new List<Vector3>();
    private List<Vector3> AerialSpawnPoints = new List<Vector3>();

    void Awake() {
        SetTerrainSpawnPoints();
        SetAerialSpawnPoints();
        PoolTerrainTurrets();
        PoolAerialTurrets();
    }

    void SetTerrainSpawnPoints() {
        for (int i = 0; i < terrainPositions.Count; i++) {
            TerrainSpawnPoints.Add(terrainPositions[i].transform.position + terrainOffset);
        }
    }

    void SetAerialSpawnPoints() {
        for (int i = 0; i < aerialPositions.Count; i++) {
            AerialSpawnPoints.Add(aerialPositions[i].transform.position + aerialOffset);
        }
    }

    void PoolTerrainTurrets() {
        int terrain = (int)Turret.TurretType.AntiTerrain;

        for (int i = 0; i < terrainPositions.Count; i++) {
            PooledTurrets.Add(Instantiate(TurretTypes[terrain], TerrainSpawnPoints[i], Quaternion.identity, transform) as Turret);
        }
    }

    void PoolAerialTurrets() {
        int aerial = (int)Turret.TurretType.AntiAerial;

        for (int i = 0; i < aerialPositions.Count; i++) {
            PooledTurrets.Add(Instantiate(TurretTypes[aerial], AerialSpawnPoints[i], Quaternion.identity, transform) as Turret);
        }
    }
}
