﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileSpawner : MonoBehaviour {
    public List<Projectile> ProjectileTypes = new List<Projectile>();
    public List<Projectile> PooledProjectiles = new List<Projectile>();

    [SerializeField]
    protected TurretSpawner turrets;
    [SerializeField]
    protected Vector3 terrainNozzle;
    [SerializeField]
    protected Vector3 aerialNozzle;
    
    [SerializeField]
    private List<Vector3> SpawnPoints = new List<Vector3>();
    private int maximumPool = 5;

    void Awake() {
        Invoke("SetSpawnPoints", 0.1f);
        Invoke("PoolProjectiles", 0.1f);
    }

    void SetSpawnPoints() {
        for (int i = 0; i < turrets.PooledTurrets.Count; i++) {
            if (turrets.PooledTurrets[i].Type == Turret.TurretType.AntiTerrain)
                SpawnPoints.Add(turrets.PooledTurrets[i].transform.position + terrainNozzle);
            if (turrets.PooledTurrets[i].Type == Turret.TurretType.AntiAerial)
                SpawnPoints.Add(turrets.PooledTurrets[i].transform.position + aerialNozzle);
        }
    }

    void PoolProjectiles() {
        int bullet = (int)Projectile.ProjectileType.Bullet;
        int rocket = (int)Projectile.ProjectileType.Rocket;

        for (int i = 0; i < turrets.PooledTurrets.Count; i++) {
            for (int j = 0; j < maximumPool; j++) {
                if (turrets.PooledTurrets[i].Type == Turret.TurretType.AntiTerrain) {
                    PooledProjectiles.Add(Instantiate(ProjectileTypes[bullet], SpawnPoints[i], Quaternion.identity) as Projectile);
                }
                if (turrets.PooledTurrets[i].Type == Turret.TurretType.AntiAerial) {
                    PooledProjectiles.Add(Instantiate(ProjectileTypes[rocket], SpawnPoints[i], Quaternion.identity) as Projectile);
                }
                PooledProjectiles[j + i * maximumPool].transform.SetParent(turrets.PooledTurrets[i].transform.GetChild(2), true);
                PooledProjectiles[j + i * maximumPool].gameObject.SetActive(false);
            }
        }
    }
}
