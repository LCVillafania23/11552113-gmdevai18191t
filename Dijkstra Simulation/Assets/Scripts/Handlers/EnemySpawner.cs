﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {
    public List<Follower> PooledEnemies = new List<Follower>();
    public float SpawnInterval;

    [SerializeField]
    protected Graph ground;
    [SerializeField]
    protected Graph air;

    [SerializeField]
    private int maxPooledEnemies;

    void Awake() {
        CreatePool();
        ManagePool();
        StartCoroutine(SpawnEnemies());
    }

    void CreatePool() {
        Vector3 groundStart = ground.getNodes[0].transform.position;
        Vector3 airStart = air.getNodes[0].transform.position;

        for (int i = 0; i < maxPooledEnemies / 2; i++) {
            int randomGroundEnemy = Random.Range(0, ground.m_Enemies.Count);
            int randomAirEnemy = Random.Range(0, air.m_Enemies.Count);
            PooledEnemies.Add(Instantiate(ground.m_Enemies[randomGroundEnemy], groundStart, Quaternion.identity, transform) as Follower);
            PooledEnemies.Add(Instantiate(air.m_Enemies[randomAirEnemy], airStart, Quaternion.identity, transform) as Follower);
        }
    }

    void ManagePool() {
        for (int i = 0; i < maxPooledEnemies; i++) {
            PooledEnemies[i].GetComponent<Follower>().enabled = false;
            PooledEnemies[i].gameObject.SetActive(false);
        }
    }

    IEnumerator SpawnEnemies() {
        for (int i = 0; i < maxPooledEnemies; i++) {
            yield return new WaitForSeconds(SpawnInterval);
            PooledEnemies[i].GetComponent<Follower>().enabled = true;
            PooledEnemies[i].gameObject.SetActive(true);

            if (i == maxPooledEnemies - 1)
                i = 0;
        }
    }
}
