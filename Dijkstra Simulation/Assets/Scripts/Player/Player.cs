﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerAttributes {
    public int MaximumHealth;
    public int AmmoAllocation;
}

public class Player : MonoBehaviour {
    public PlayerAttributes Attributes;
    public int CurrentHealth;
    public int AvailableAmmo;

    private float healthPercentage;
    private float ammoPercentage;
    private int replenishAmount = 20;
    private bool hasReplenished;

    void Awake() {
        CurrentHealth = Attributes.MaximumHealth;
        AvailableAmmo = Attributes.AmmoAllocation;
    }

    void Update() {
        SetPercentage();
        ReplenishAmmoOverTime();
    }

    void SetPercentage() {
        healthPercentage = (float)CurrentHealth / (float)Attributes.MaximumHealth;
        ammoPercentage = (float)AvailableAmmo / (float)Attributes.AmmoAllocation;
    }

    void ReplenishAmmoOverTime() {
        if (AvailableAmmo <= Attributes.AmmoAllocation - replenishAmount && !hasReplenished)
            StartCoroutine(ReplenishAmmo());
    }

    IEnumerator ReplenishAmmo() {
        float replenishInterval = 3f;

        hasReplenished = true;
        AvailableAmmo += replenishAmount;
        yield return new WaitForSeconds(replenishInterval);

        hasReplenished = false;
        yield return null;
    }

    public float GetHealthPercentage() {
        return healthPercentage;
    }
    
    public float GetAmmoPercentage() {
        return ammoPercentage;
    }

    public void AllocateAmmo(int ammoToAllocate) {
        AvailableAmmo += ammoToAllocate;
    }

    public void DamagePlayer(int damage) {
        CurrentHealth -= damage;
    }
}
