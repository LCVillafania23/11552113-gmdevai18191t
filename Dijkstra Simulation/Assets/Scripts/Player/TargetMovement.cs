﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetMovement : MonoBehaviour {
    public float MoverSpeed;

    [SerializeField]
    protected InputManager Manager;
    [SerializeField]
    protected Vector3 MoverOffset;

    void Update() {
        MoveTarget();
    }

    void MoveTarget() {
        transform.Translate(Manager.CheckDirection() * MoverSpeed * Time.deltaTime);
    }    
}
