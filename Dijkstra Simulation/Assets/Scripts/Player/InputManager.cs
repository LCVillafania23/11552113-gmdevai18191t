﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour {
    public Vector3 Direction;
    private Player player;

    void Awake() {
        player = GetComponentInChildren<Player>();
    }

    void Update() {
        CheckDirection();
        CheckMouseInput();
    }

    public Vector3 CheckDirection() {
        if (Input.GetButtonUp("MoveForward") || Input.GetButtonUp("MoveBackward") || Input.GetButtonUp("MoveLeftward") || Input.GetButtonUp("MoveRightward"))
            Direction = Vector3.zero;
        if (Input.GetButton("MoveForward"))
            Direction += new Vector3(0f, 0f, 1f);
        if (Input.GetButton("MoveBackward"))
            Direction += new Vector3(0f, 0f, -1f);
        if (Input.GetButton("MoveLeftward"))
            Direction += new Vector3(-1f, 0f, 0f);
        if (Input.GetButton("MoveRightward"))
            Direction += new Vector3(1f, 0f, 0f);
        Direction.Normalize();

        return Direction;
    }

    void CheckMouseInput() {
        Turret selectedTurret = null;
        Vector3 mouseInput = Input.mousePosition;
        int fillAmount = 10;

        Ray ray = Camera.main.ScreenPointToRay(mouseInput);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 100f)) {
            if (Input.GetButtonDown("Fill") && hit.collider.tag == "Turret" && player.AvailableAmmo >= fillAmount) {
                selectedTurret = hit.collider.gameObject.GetComponent<Turret>();

                if (selectedTurret.CurrentAmmo <= selectedTurret.Attributes.MaximumAmmo - fillAmount) {
                    player.AllocateAmmo(-fillAmount);
                    selectedTurret.AddAmmo(fillAmount);
                }
            }
            if (Input.GetButtonDown("Empty") && hit.collider.tag == "Turret" && player.AvailableAmmo <= player.Attributes.AmmoAllocation) {
                selectedTurret = hit.collider.gameObject.GetComponent<Turret>();

                if (selectedTurret.CurrentAmmo >= fillAmount) {
                    player.AllocateAmmo(fillAmount);
                    selectedTurret.RemoveAmmo(fillAmount);
                }
            }
        }
    }
}
