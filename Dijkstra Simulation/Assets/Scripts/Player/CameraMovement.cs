﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {
    public Transform LookAt;
    public Vector3 Offset;
    private Vector3 velocity = Vector3.zero;
    private float delay = 0.15f;
    
    void Update() {
        Vector3 desiredPosition = LookAt.transform.position + Offset;
        transform.position = Vector3.SmoothDamp(transform.position, desiredPosition, ref velocity, delay);
    }
}
