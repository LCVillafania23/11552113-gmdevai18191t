﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecrementHealth : MonoBehaviour {
    [SerializeField]
    private Player player;

    void OnTriggerEnter(Collider enemy) {
        if (enemy.tag == "GroundUnit" || enemy.tag == "AirUnit") {
            enemy.GetComponent<Enemy>().CurrentHealth = 0;
            player.DamagePlayer(enemy.GetComponent<Enemy>().Attributes.AttackDamage);
        }
    }
}
