﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileBehavior : MonoBehaviour {
    public Vector3 Origin;
    [SerializeField]
    protected Projectile projectile;

    private TurretAttack turretAttack;
    private TurretTargeter turretTargeter;
    private float startTime;

    void Awake() {
        Origin = transform.position;
    }

    void Start() {
        turretAttack = GetComponentInParent<TurretAttack>();
        turretTargeter = transform.parent.parent.GetComponentInChildren<TurretTargeter>();
        startTime = Time.time;
    }

    void Update() {
        if (turretTargeter.GetChosenTarget() == null)
            gameObject.SetActive(false);
        if (turretTargeter.GetChosenTarget() != null) {
            transform.position = Vector3.Lerp(transform.position, turretTargeter.GetChosenTarget().transform.position, projectile.Attributes.ProjectileSpeed * Time.deltaTime);
            transform.LookAt(turretTargeter.GetChosenTarget().transform.position);
        }
    }

    void OnCollisionEnter(Collision target) {
        if (target.gameObject.tag == "GroundUnit" && turretTargeter.GetChosenTarget() != null || target.gameObject.tag == "AirUnit" && turretTargeter.GetChosenTarget() != null) {
            gameObject.SetActive(false);
            transform.position = Origin;
            target.gameObject.GetComponent<Enemy>().DamageEnemy(projectile.Attributes.ProjectileDamage);
        }
    }
}
