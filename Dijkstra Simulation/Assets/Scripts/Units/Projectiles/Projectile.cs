﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ProjectileAttributes {
    public int ProjectileDamage;
    public float ProjectileSpeed;
}

public class Projectile : MonoBehaviour {
    public enum ProjectileType {
        Bullet,
        Rocket
    }
    public ProjectileType Type;
    public ProjectileAttributes Attributes;
}
