﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretAttack : MonoBehaviour {
    [SerializeField]
    protected TurretTargeter turretTargeter;

    private Turret turret;
    private Projectile projectile;
    private bool hasAttacked;
    private int iterator = 0;
    private int maximumPool = 5;

    void Awake() {
        turret = GetComponent<Turret>();
        projectile = GetComponentInChildren<Projectile>();
    }

    void Update() {
        if (turretTargeter.GetChosenTarget() != null && turretTargeter.GetChosenTarget().isActiveAndEnabled && turret.CurrentAmmo > turret.Empty) {
            transform.GetChild(2).LookAt(turretTargeter.GetChosenTarget().transform);

            if (!hasAttacked)
                StartCoroutine(ShootProjectiles());
            if (iterator >= maximumPool - 1)
                iterator = 0;
        }
    }

    public IEnumerator ShootProjectiles() {
        float intervalModifier = 10f;
        float attackInterval = intervalModifier / turret.Attributes.AttackSpeed;

        hasAttacked = true;
        turret.CurrentAmmo -= 1;
        gameObject.transform.GetChild(2).GetChild(iterator + 1).gameObject.SetActive(true);
        yield return new WaitForSeconds(attackInterval);

        iterator++;
        hasAttacked = false;
        yield return null;
    }
}
