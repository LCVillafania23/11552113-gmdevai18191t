﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretTargeter : MonoBehaviour {
    public float Distance;

    [SerializeField]
    protected Turret turret;

    [SerializeField]
    private List<Enemy> possibleTargets = new List<Enemy>();
    [SerializeField]
    private Enemy chosenTarget;

    public Enemy GetChosenTarget() {
        if (possibleTargets.Count == 0) {
            chosenTarget = null;
            Distance = 0f;
        }
        if (possibleTargets.Count != 0) {
            chosenTarget = possibleTargets[possibleTargets.Count - 1];

            if (chosenTarget != null)
                Distance = Vector3.Distance(chosenTarget.transform.position, transform.position);
        }
        return chosenTarget;
    }

    void OnTriggerEnter(Collider target) {
        if (target.gameObject.tag == "GroundUnit" && turret.Type == Turret.TurretType.AntiTerrain)
            possibleTargets.Add(target.GetComponent<Enemy>());
        if (target.gameObject.tag == "AirUnit" && turret.Type == Turret.TurretType.AntiAerial)
            possibleTargets.Add(target.GetComponent<Enemy>());
    }

    void OnTriggerExit(Collider target) {
        if (target.gameObject.tag == "GroundUnit") {
            possibleTargets.Remove(target.GetComponent<Enemy>());
        }
        if (target.gameObject.tag == "AirUnit") {
            possibleTargets.Remove(target.GetComponent<Enemy>());
        }
    }
}
