﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TurretAttributes {
    public int MaximumAmmo;
    public float AttackSpeed;
}

public class Turret : MonoBehaviour {
    public enum TurretType {
        AntiTerrain = 0,
        AntiAerial = 1
    }
    public TurretType Type;
    public TurretAttributes Attributes;
    public int CurrentAmmo;
    [HideInInspector]
    public int Empty = 0;

    private float ammoPercentage;

    void Awake() {
        CurrentAmmo = Empty;
    }

    void Update() {
        SetAmmoPercentage();
    }

    void SetAmmoPercentage() {
        ammoPercentage = (float)CurrentAmmo / (float)Attributes.MaximumAmmo;
    }

    public float GetAmmoPercentage() {
        return ammoPercentage;
    }

    public void AddAmmo(int fillAmount) {
        if (CurrentAmmo <= Attributes.MaximumAmmo - fillAmount)
            CurrentAmmo += fillAmount;
    }

    public void RemoveAmmo(int fillAmount) {
        if (CurrentAmmo >= fillAmount)
            CurrentAmmo -= fillAmount;
    }
}
