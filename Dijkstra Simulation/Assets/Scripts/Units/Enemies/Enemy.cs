﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EnemyAttributes {
    public int PointsGranted;
    public int AttackDamage;
    public int MaximumHealth;
    public float MovementSpeed;
    public float RotationSpeed;
}

public class Enemy : MonoBehaviour {
    public enum EnemyType {
        Ground,
        Air
    }
    public EnemyType Type;
    public EnemyAttributes Attributes;
    public int CurrentHealth;
    private float healthPercentage;

    void Awake() {
        CurrentHealth = Attributes.MaximumHealth;
    }

    void Update() {
        SetHealthPercentage();
    }

    void SetHealthPercentage() {
        healthPercentage = (float)CurrentHealth / (float)Attributes.MaximumHealth;
    }

    public float GetHealthPercentage() {
        return healthPercentage;
    }

    public void DamageEnemy(int damage) {
         CurrentHealth -= damage;
    }

    public bool OnDeath() {
        if (CurrentHealth <= 0)
            return true;
        else
            return false;
    }
}