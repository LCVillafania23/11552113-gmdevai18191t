﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehavior : MonoBehaviour {
    [SerializeField]
    private Enemy enemy;
    [SerializeField]
    private GameObject groundStart;
    [SerializeField]
    private GameObject airStart;

    void Update() {
        if (enemy != null) {
            if (enemy.OnDeath()) {
                enemy.CurrentHealth = 0;
                ResetPosition();
            }
        }
    }

    void ResetPosition() {
        if (enemy.Type == Enemy.EnemyType.Ground) {
            gameObject.SetActive(false);
            transform.position = groundStart.transform.position;
        }
        if (enemy.Type == Enemy.EnemyType.Air) {
            gameObject.SetActive(false);
            transform.position = airStart.transform.position;
        }
    }
}
