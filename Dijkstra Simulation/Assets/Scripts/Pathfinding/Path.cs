﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Path {
    protected List<Node> nodes = new List<Node>();
    protected float length = 0f;

    public virtual List<Node> getNodes {
        get { return nodes; }
    }

    public virtual float getLength {
        get { return length;  }
    }

    public virtual void Bake() {
        List<Node> calculated = new List<Node>();
        length = 0;

        for (int i = 0; i < nodes.Count; i++) {
            Node node = nodes[i];

            for (int j = 0; j < node.getConnections.Count; j++) {
                Node connection = node.getConnections[j];

                if (nodes.Contains(connection) && !calculated.Contains(connection)) {
                    length += Vector3.Distance(node.transform.position, connection.transform.position);
                }
                calculated.Add(node);
            }
        }
    }

    public override string ToString() {
        return string.Format("Nodes: [0]\nLength: {1}", ", ", nodes.Select(node => node.name).ToArray(), length);
    }
}
