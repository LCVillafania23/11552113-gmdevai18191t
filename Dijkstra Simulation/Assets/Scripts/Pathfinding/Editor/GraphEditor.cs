﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;

[CustomEditor(typeof(Graph))]

public class GraphEditor : Editor {
    protected Graph m_Graph;
    protected Node m_From;
    protected Node m_To;
    protected Path m_Path = new Path();

    private void OnEnable() {
        m_Graph = target as Graph;
    }

    private void OnSceneGUI() {
        if (m_Graph == null) return;

        for (int i = 0; i < m_Graph.getNodes.Count; i++) {
            Node node = m_Graph.getNodes[i];

            for (int j = 0; j < node.getConnections.Count; j++) {
                Node connection = node.getConnections[j];

                if (connection == null) {
                    continue;
                }
                float distance = Vector3.Distance(node.transform.position, connection.transform.position);
                Vector3 difference = connection.transform.position - node.transform.position;
                Handles.Label(node.transform.position + (difference / 2), distance.ToString(), EditorStyles.whiteBoldLabel);

                if (m_Path.getNodes.Contains(node) && m_Path.getNodes.Contains(connection)) {
                    Color color = Handles.color;
                    Handles.color = Color.green;
                    Handles.DrawLine(node.transform.position, connection.transform.position);
                    Handles.color = color;
                }
                else {
                    Handles.DrawLine(node.transform.position, connection.transform.position);
                }
            }
        }
    }

    public override void OnInspectorGUI() {
        m_Graph.getNodes.Clear();
        foreach (Transform child in m_Graph.transform) {
            Node node = child.GetComponent<Node>();

            if (node != null) {
                m_Graph.getNodes.Add(node);
            }
        }
        base.OnInspectorGUI();
        EditorGUILayout.Separator();

        m_From = m_Graph.getNodes[0];
        m_To = m_Graph.getNodes[m_Graph.getNodes.Count - 1];

        if (GUILayout.Button("Show Shortest Path")) {
            m_Path = m_Graph.GetDijkstraPath(m_From, m_To);

            if (m_Graph.m_Enemies != null) {
                foreach (Follower enemies in m_Graph.m_Enemies) {
                    enemies.FollowPath(m_Path);
                }
            }
            SceneView.RepaintAll();
        }
    }
}