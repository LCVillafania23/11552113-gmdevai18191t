﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Graph : MonoBehaviour {
    public virtual List<Node> getNodes
    {
        get { return nodes; }
    }
    public List<Follower> m_Enemies = new List<Follower>();

    [SerializeField]
    protected List<Node> nodes = new List<Node>();
    [SerializeField]
    public EnemySpawner Spawner;

    public virtual Path GetDijkstraPath(Node start, Node end) {
        if (start == null || end == null) {
            throw new ArgumentNullException();
        }
        Path path = new Path();

        if (start == end) {
            path.getNodes.Add(start);
            return path;
        }
        List<Node> unvisited = new List<Node>();
        Dictionary<Node, Node> previous = new Dictionary<Node, Node>();
        Dictionary<Node, float> distances = new Dictionary<Node, float>();

        for (int i = 0; i < nodes.Count; i++) {
            Node node = nodes[i];
            unvisited.Add(node);
            distances.Add(node, float.MaxValue);
        }
        distances[start] = 0f;

        while (unvisited.Count != 0) {
            unvisited = unvisited.OrderBy(node => distances[node]).ToList();

            Node current = unvisited[0];
            unvisited.Remove(current);

            if (current == end) {
                while (previous.ContainsKey(current)) {
                    path.getNodes.Insert(0, current);
                    current = previous[current];
                }
                path.getNodes.Insert(0, current);
                break;
            }
            for (int i = 0; i < current.getConnections.Count; i++) {
                Node neighbor = current.getConnections[i];
                float length = Vector3.Distance(current.transform.position, neighbor.transform.position);
                float temp = distances[current] = length;

                if (temp < distances[neighbor]) {
                    distances[neighbor] = temp;
                    previous[neighbor] = current;
                }
            }
        }
        path.Bake();
        return path;
    }
}
