﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follower : MonoBehaviour {
    [SerializeField]
    protected Graph graph;
    [SerializeField]
    protected Node start;
    [SerializeField]
    protected Node end;
    [SerializeField]
    protected Path path = new Path();
    protected Node current;

    private EnemySpawner enemySpawner;
    private Enemy enemy;
    private Vector3 targetDirection;
    private Vector3 newDirection;
    private float speed;

    void Awake() {
        enemySpawner = transform.parent.GetComponent<EnemySpawner>();
        enemy = GetComponent<Enemy>();
    }

    void Start() {
        float speedMultiplier = 0.001f;
        speed = enemy.Attributes.MovementSpeed * speedMultiplier;
        path = graph.GetDijkstraPath(start, end);
        FollowPath(path);
    }

    public void FollowPath(Path path) {
        if (this.isActiveAndEnabled) {
            StopCoroutine(MoveTowardsPath());
            this.path = path;
            transform.position = path.getNodes[0].transform.position;
            StartCoroutine(MoveTowardsPath());
        }
    }

    IEnumerator MoveTowardsPath() {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.update += Update;
#endif
        var e = path.getNodes.GetEnumerator();

        while (e.MoveNext()) {
            current = e.Current;
            yield return new WaitUntil(() => {
                return transform.position == current.transform.position;
            });
        }
        current = null;
#if UNITY_EDITOR
        UnityEditor.EditorApplication.update -= Update;
#endif
    }

    void Update() {
        if (current != null && enemy != null) {
            targetDirection = current.transform.position - transform.position;
            newDirection = Vector3.RotateTowards(transform.forward, targetDirection, enemy.Attributes.RotationSpeed * Time.deltaTime, 0f);
            transform.position = Vector3.MoveTowards(transform.position, current.transform.position, speed);
            transform.rotation = Quaternion.LookRotation(newDirection);

            if (enemy.OnDeath()) {
                enemy.CurrentHealth = enemy.Attributes.MaximumHealth;
                ResetPosition();
                ReorderPool();
            }
        }
    }

    void ResetPosition() {
        current = start;
        transform.position = current.transform.position;
    }

    void ReorderPool() {
        enemySpawner.PooledEnemies.Add(this);
        gameObject.GetComponent<Follower>().enabled = false;
        gameObject.SetActive(false);
        enemySpawner.PooledEnemies.Remove(this);
    }
}
