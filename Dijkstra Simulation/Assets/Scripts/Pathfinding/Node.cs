﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour {
    [SerializeField]
    protected List<Node> connections = new List<Node>();

    public virtual List<Node> getConnections
    {
        get { return connections; }
    }
    public Node this [int index] {

        get { return connections[index]; }
    }

    private void OnValidate() {
        connections = connections.Distinct().ToList();
    }
}