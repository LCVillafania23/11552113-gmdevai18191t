﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/Escape")]
public class EscapeAction : Action
{
    public override void Act(StateController controller)
    {
        Escape(controller);
    }

    private void Escape(StateController controller)
    {
        controller.navMeshAgent.destination = controller.escapePointList[controller.nextWayPoint].position;
        controller.navMeshAgent.Resume();

        if (controller.navMeshAgent.remainingDistance <= controller.navMeshAgent.stoppingDistance && !controller.navMeshAgent.pathPending)
        {
            controller.nextWayPoint = (controller.nextWayPoint + 1) % controller.escapePointList.Count;
        }
    }
}