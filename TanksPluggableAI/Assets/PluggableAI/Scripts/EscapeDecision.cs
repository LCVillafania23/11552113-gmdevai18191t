﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/Escape")]
public class EscapeDecision : Decision
{
    public override bool Decide(StateController controller)
    {
        bool healthCritical = Escape(controller);
        return healthCritical;
    }

    private bool Escape(StateController controller)
    {
        if (controller.gameObject.GetComponent<Complete.TankHealth>().OnCritical())
            return true;
        else
            return false;
    }
}